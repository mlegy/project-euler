#Problem 4 :
#A palindromic number reads the same both ways.
#The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
#Find the largest palindrome made from the product of two 3-digit numbers.

#Solution :

palindrome = 0
for i in range(100,999):
    for j in range (100,999):
        tmp = i * j
        res = str(tmp)
        if (res[0] == res[len(res)-1] and res[1] == res[len(res)-2] and res[len(res)-3] == res[len(res)-4]):
            if (tmp > palindrome):
                palindrome = tmp
print(palindrome)
